# CISS - Solution Designer

Link to Hosted application: https://solution-designer.herokuapp.com/


## Folder Structure
```
├── README.md
├── connections-designer  // UI- development repo
│   ├── src               // src code for UI development
│   ├── public
│   └── package.json
├── app.js
├── public                // contains built UI resources
├── package.json
└── server.js
```
