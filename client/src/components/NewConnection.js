import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { Formik, Form, Field } from 'formik';
import CustomModal from './CustomModal';
import { connect } from 'react-redux';
import { toggleModal } from './../redux/actions/modal-actions';
import { setDiagram } from './../redux/actions/diagram-actions';
import { DefaultLabelModel } from 'storm-react-diagrams';
import Creatable from 'react-select/lib/Creatable';
import PropTypes from 'prop-types';
class NewConnection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };

    this.data = {
      types: [{ name: 'Global HR System', value: 'ghrs' }],
      vendors: [{ name: 'SAP SuccessFactors EC', value: 'sapsfec' }],
      entity: [
        { value: 'employee', label: 'Employee' },
        { value: 'orgObjects', label: 'Org Objects' },
      ],
      countries: [
        { value: 'India', label: 'India' },
        { value: 'US', label: 'US' },
        { value: 'Canada', label: 'Canada' },
      ],
    };
  }

  getPassedData = () => {
    const { modal } = this.props;
    const selector_id = 'new_connection-data';
    const passedData = selector_id in modal ? modal[selector_id].id : null;
    return passedData;
  };

  getDetails = () => {
    try {
      if (this.props.diagram) {
        const passed_id = this.getPassedData();
        if (!passed_id) return;

        const selected_link = this.props.diagram.links.filter(item => item.id == passed_id);

        const source_id = selected_link[0].source;
        const source_port_id = selected_link[0].sourcePort;

        const target_id = selected_link[0].target;
        const target_port_id = selected_link[0].targetPort;

        console.log(source_id);

        const source_node = this.props.diagram.nodes.filter(item => item.id == source_id)[0];
        const target_node = this.props.diagram.nodes.filter(item => item.id == target_id)[0];

        const source_port = source_node.ports.filter(item => item.id == source_port_id)[0];
        const target_port = target_node.ports.filter(item => item.id == target_port_id)[0];

        return { source_node, target_node, selected_link, passed_id, source_port, target_port };
      }
    } catch (error) {}
  };
  saveToStore = formData => {
    if (this.props.diagram) {
      const details = this.getDetails();
      const { disc } = formData;
      const link = this.props.diagram.links.filter(link => link.id === details.passed_id)[0];
      const newLabel = new DefaultLabelModel();
      newLabel.setLabel(disc);
      link.labels.push(newLabel.serialize());
    }

    this.props.setDiagram(this.props.diagram);
    this.props.onRefresh();
  };

  render() {
    const details = this.getDetails();
    return (
      <div>
        <CustomModal
          name="new_connection"
          heading={'Create New Connection'}
          className={this.props.className}
        >
          <ModalBody>
            {details ? (
              <div>
                <div className="row">
                  <div className="col-2">System</div>
                  <div className="col">
                    <h6 className="new_connection_head">
                      {details.source_node.name}
                      <i class="fas fa-arrows-alt-h px-4" />
                      {details.target_node.name}
                    </h6>
                  </div>
                </div>
                <div className="row">
                  <div className="col-2">Entity</div>
                  <div className="col">
                    <h6 className="new_connection_head">
                      {details.source_port.label}
                      <i class="fas fa-long-arrow-alt-right px-4" />

                      {details.target_port.label}
                    </h6>
                  </div>
                </div>
              </div>
            ) : (
              ''
            )}
            <Formik
              onSubmit={values => {
                this.saveToStore(values);
              }}
            >
              {({ submitForm, setFieldValue }) => {
                this.submitForm = submitForm;
                return (
                  <Form>
                    <div className="form-group row">
                      <label htmlFor="inputName" className="col-sm-2 col-form-label">
                        Description
                      </label>
                      <div className="col-sm-10">
                        <Field component="textarea" className="form-control" name="disc" />
                      </div>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </ModalBody>
          <ModalFooter>
            <Button
              color="primary"
              onClick={() => {
                this.submitForm();
                this.props.toggleModal('new_connection');
              }}
            >
              Save
            </Button>{' '}
            <Button color="secondary" onClick={() => this.props.toggleModal('new_connection')}>
              Cancel
            </Button>
          </ModalFooter>
        </CustomModal>
      </div>
    );
  }
}

NewConnection.defaultProps = {
  save: () => {},
};

NewConnection.propTypes = {
  onRefresh: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({ modal: state.modal, diagram: state.diagram.diagram });

const mapDispatchToProps = { toggleModal, setDiagram };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewConnection);
