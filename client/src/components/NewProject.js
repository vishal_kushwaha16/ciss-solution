import React, { Component } from 'react';
import CustomModal from './CustomModal';

import { ModalBody, ModalFooter } from 'reactstrap';
import StepZilla from './Steps';
import './../style/steps.css';
import Creatable from 'react-select/lib/Creatable';
import { createProject } from './../redux/actions/diagram-actions';
import { connect } from 'react-redux';

import FunctionsData from './../mockdata/converter/functions.json';
import SystemsData from './../mockdata/converter/functions_and_systems.json';
import EntitysData from './../mockdata/converter/function_and_entities.json';
import { toggleModal } from './../redux/actions/modal-actions';

class NewProject extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      function_map: [],
      systems_current: {},
      systems_new: {},
      entity: {},
    };
  }

  handleChange = (name, type = 'normal') => e => {
    if (type === 'normal') {
      this.setState({ [name]: e.target.value });
    } else {
      this.setState({ [name]: e });
    }
  };

  handleChangeSystems = (name_fun, system_name) => val => {
    this.setState({ [system_name]: { ...this.state[system_name], [name_fun]: val } });
  };

  Step1 = () => (
    <div>
      <div className="form-group row">
        <label htmlFor="inputName" className="col-sm-4 col-form-label">
          Project Name
        </label>
        <div className="col-sm-8">
          <input
            onChange={this.handleChange('name')}
            type="text"
            class="form-control"
            id="p_name"
            placeholder="Name your project ..."
          />
        </div>
      </div>
      <div className="form-group row">
        <label htmlFor="inputName" className="col-sm-4 col-form-label">
          Functions
        </label>
        <div className="col-sm-8">
          <Creatable
            onChange={this.handleChange('function_map', 'select')}
            options={FunctionsData}
            isMulti={true}
          />
        </div>
      </div>
    </div>
  );

  Step2 = () => (
    <div
      style={{
        display: 'flex',
        justifyContent: 'space-between',
      }}
    >
      <div style={{ width: '350px' }}>
        <h6 className="text-muted mb-2">Current Systems </h6>
        {this.state.function_map.map((single_fun_map, key) => (
          <div key={key} className="form-group">
            <label htmlFor="inputName" className=" col-form-label">
              {single_fun_map.label}
            </label>
            <div>
              <Creatable
                onChange={this.handleChangeSystems(single_fun_map.value, 'systems_current')}
                options={SystemsData[single_fun_map.value]}
                placeholder={`System for ${single_fun_map.label}`}
              />
            </div>
          </div>
        ))}
      </div>
      <div style={{ width: '350px' }}>
        <h6 className="text-muted mb-2">New Systems</h6>
        <div>
          {this.state.function_map.map((single_fun_map, key) => (
            <div key={key} className="form-group">
              <label htmlFor="inputName" className=" col-form-label">
                {single_fun_map.label}
              </label>
              <div>
                <Creatable
                  defaultValue=""
                  onChange={this.handleChangeSystems(single_fun_map.value, 'systems_new')}
                  options={SystemsData[single_fun_map.value]}
                  placeholder={`System for ${single_fun_map.label}`}
                />
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );

  createProject = () => {
    const entity = {};
    this.state.function_map.map(data => {
      entity[data.value] = EntitysData[data.value];
    });
    //this.state.entity = entity;
    this.props.createProject({ ...this.state, entity });
    this.props.toggleModal('new_project');
  };

  render() {
    const steps = [
      { name: 'Project Information', component: this.Step1() },
      { name: 'Landscape mapping', component: this.Step2() },
    ];

    return (
      <CustomModal size={'lg'} name="new_project" heading={'New Project'}>
        <ModalBody>
          <div className="step-progress">
            <StepZilla
              nextTextOnFinalActionStep={'Create'}
              dontValidate={true}
              steps={steps}
              complete={this.createProject}
            />
          </div>
        </ModalBody>
      </CustomModal>
    );
  }
}
const mapStateToProps = state => ({});

const mapDispatchToProps = { createProject, toggleModal };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewProject);
