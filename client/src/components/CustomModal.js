import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, ModalHeader } from 'reactstrap';
import { toggleModal } from './../redux/actions/modal-actions';

export class CustomModal extends Component {
  render() {
    const { name, modal, heading, toggleModal, ...rest } = this.props;
    const togg = name in modal ? modal[name] : false;
    return (
      <Modal isOpen={togg} toggle={() => toggleModal(name)} {...rest}>
        <ModalHeader toggle={() => toggleModal(name)}>{heading}</ModalHeader>

        {this.props.children}
      </Modal>
    );
  }
}

const mapStateToProps = state => ({ modal: state.modal });

const mapDispatchToProps = { toggleModal };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomModal);
