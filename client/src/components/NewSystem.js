import React from 'react';
import { Button, ModalBody, ModalFooter } from 'reactstrap';
import { Formik, Form, Field } from 'formik';
import Creatable from 'react-select/lib/Creatable';
import CustomModal from './CustomModal';
import { connect } from 'react-redux';

import { toggleModal } from './../redux/actions/modal-actions';

import FunctionsData from './../mockdata/converter/functions.json';
import SystemsData from './../mockdata/converter/functions_and_systems.json';
import EntitysData from './../mockdata/converter/function_and_entities.json';

class NewSystem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
    };

    this.data = {
      countries: [
        { value: 'India', label: 'India' },
        { value: 'US', label: 'US' },
        { value: 'Canada', label: 'Canada' },
      ],
    };
  }

  render() {
    return (
      <div>
        <CustomModal
          name="new_system"
          heading={'Create New System'}
          className={this.props.className}
        >
          <ModalBody>
            <Formik
              initialValues={{
                name: '',
                vendor: '',
                type: '',
                entity: [],
                countries: [],
              }}
              onSubmit={values => {
                this.props.save(values);
              }}
            >
              {({ submitForm, setFieldValue, values }) => {
                this.save = submitForm;
                return (
                  <Form>
                    <div className="form-group row">
                      <label htmlFor="inputName" className="col-sm-2 col-form-label">
                        Name
                      </label>
                      <div className="col-sm-10">
                        <Field type="text" name="name" className="form-control" id="inputName" />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label htmlFor="inputType" className="col-sm-2 col-form-label">
                        Type
                      </label>

                      <div className="col-sm-10">
                        <Creatable
                          onChange={value => setFieldValue('type', value)}
                          options={FunctionsData}
                          // isMulti={true}
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label htmlFor="inputVendor" className="col-sm-2 col-form-label">
                        Vendor
                      </label>

                      <div className="col-sm-10">
                        <Creatable
                          onChange={value => setFieldValue('vendor', value)}
                          options={SystemsData[values.type.value]}
                          // isMulti={true}
                          isDisabled={!values.type}
                        />
                      </div>
                    </div>

                    <div className="form-group row">
                      <label htmlFor="inputName" className="col-sm-2 col-form-label">
                        Entity
                      </label>
                      <div className="col-sm-10">
                        <Creatable
                          onChange={value => setFieldValue('entity', value)}
                          options={EntitysData[values.type.value]}
                          isDisabled={!values.vendor}
                          isMulti={true}
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <label htmlFor="inputName" className="col-sm-2 col-form-label">
                        Countries
                      </label>
                      <div className="col-sm-10">
                        <Creatable
                          onChange={value => setFieldValue('countries', value)}
                          options={this.data.countries}
                          isMulti={true}
                        />
                      </div>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </ModalBody>
          <ModalFooter>
            <Button
              color="primary"
              onClick={() => {
                this.props.toggleModal('new_system');
                this.save();
              }}
            >
              Save
            </Button>{' '}
            <Button color="secondary" onClick={() => this.props.toggleModal('new_system')}>
              Cancel
            </Button>
          </ModalFooter>
        </CustomModal>
      </div>
    );
  }
}

NewSystem.defaultProps = {
  save: () => {},
};

const mapStateToProps = state => ({});

const mapDispatchToProps = { toggleModal };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewSystem);
