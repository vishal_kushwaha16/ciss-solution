import React, { Component } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem } from 'reactstrap';
import logo from './../assets/integrtr_logo_white.png';
import { NavLink } from 'react-router-dom';
import NewProject from './../components/NewProject';
import { connect } from 'react-redux';
import { toggleModal } from './../redux/actions/modal-actions';

class MainLayout extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  static defaultProps = {
    title: '',
  };

  render() {
    return (
      <>
        <Navbar
          color="light"
          light
          className="p-0 border-bottom d-flex justify-content-between headerNavBar"
          expand="md"
        >
          <NavLink className="navbar-brand" to="/">
            <img src={logo} alt="Integrtr" className="brand-logo integrtrLogo" />
          </NavLink>
          <div className="name h4 mb-0 appTitle">{this.props.title}</div>
          <Nav navbar>
            <NavItem>
              <button
                style={{ display: 'none' }}
                className="nav-link btn font-weight-light text-light btn-secondary px-3 py-1 mr-2"
                // to="/create_project"
                onClick={() => this.props.toggleModal('new_project')}
              >
                <i class="fa fa-file pr-2" aria-hidden="true" />
                New Project
              </button>
              <NewProject />
            </NavItem>
          </Nav>
        </Navbar>
        <div className="main">{this.props.children}</div>
      </>
    );
  }
}

const mapStateToProps = state => ({});
const mapDispatchToProps = { toggleModal };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainLayout);
