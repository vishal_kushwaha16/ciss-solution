import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from './../pages/Home';
import StartUp from './../pages/StartUp';

export default class Router extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/:id" component={Home} />
          <Route path="/" component={StartUp} />
        </Switch>
      </BrowserRouter>
    );
  }
}
