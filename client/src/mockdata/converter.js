const fs = require('fs');
const fnanden = require('./function_and_entities.json');
const fnandsys = require('./functions_and_systems.json');

const make_space_less = stringdata => {
  stringdata = stringdata.split(' ');
  stringdata = stringdata.join('_');
  stringdata = stringdata.toLowerCase();
  return stringdata;
};

let dataRtn = fnandsys.map(data => {
  data.Functions = { value: make_space_less(data.Functions), label: data.Functions };
  data.Systems = data.Systems.split(',');

  data.Systems = data.Systems.map(data_sys => {
    const tr = data_sys.trim();
    return { value: make_space_less(tr), label: tr };
  });

  return data;
});

let ObjFunSys = {};

dataRtn.map(data => {
  ObjFunSys[data.Functions.value] = data.Systems;
});

fs.writeFileSync('converter/functions_and_systems.json', JSON.stringify(ObjFunSys));

dataRtn = dataRtn.map(data => data.Functions);

fs.writeFileSync('converter/functions.json', JSON.stringify(dataRtn));

let ObjFunEnty = {};
dataRtn = fnanden.map(data => {
  //   data.Functions = { value: data.Functions, label: data.Functions };
  data.Entities = data.Entities.split(',');

  data.Entities = data.Entities.map(data_sys => {
    const tr = data_sys.trim();
    return { value: make_space_less(tr), label: tr };
  });

  ObjFunEnty[make_space_less(data.Functions)] = data.Entities;
  return data;
});

fs.writeFileSync('converter/function_and_entities.json', JSON.stringify(ObjFunEnty));
