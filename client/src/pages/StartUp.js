import React, { Component } from 'react';
import MainLayout from './../main/MainLayout';

import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { toggleModal } from './../redux/actions/modal-actions';

class StartUp extends Component {
  constructor(props) {
    super(props);
    this.color = {};
  }

  render() {
    const { toggleModal } = this.props;
    const ProjectList = [];

    for (const key in this.props.projects) {
      if (this.props.projects.hasOwnProperty(key)) {
        const element = this.props.projects[key];
        ProjectList.push(element);
      }
    }
    return (
      <MainLayout title={'Digital Transformer'}>
        <div className="row hv-100">
          <div
            className="col-2 border-right pt-4 pr-0"
            style={{
              display: 'none',
              background: '#3f5161',
              borderRight: '1px solid #536b80',
              color: 'white',
              fontSize: '0.9rem',
            }}
          >
            <div
              className="border-top border-bottom  py-3 pb-2 bg-white px-4"
              style={{ 'border-color': '#536b80 !important' }}
            >
              <i class="fas fa-star fa-md  mr-2" />
              <span className="font-weight-bold">Projects</span>
            </div>
          </div>
          <div className="col fioriBG ">
            <div>
              <h4 className="fioriTitle">Projects</h4>
            </div>
            <div
              style={{
                display: 'grid',
                gridGap: '20px',
                padding: '40px',
                paddingTop: '0px',
                gridTemplateColumns: 'repeat(auto-fill, minmax(200px, 1fr) )',
              }}
            >
              {ProjectList.map(project => (
                <Link to={`/${project.id}`}>
                  <div
                    className=" border  bg-light rounded d-flex flex-column "
                    style={{ height: '100%', maxHeight: '200px' }}
                  >
                    <div
                      className="border-top p-2 px-3 h5  text-drak  mb-0"
                      style={{
                        margin: '-1px',
                        minHeight: '20px',
                        borderRadius: '0px 0px 3px 3px',
                      }}
                    >
                      <div style={{ textTransform: 'capitalize' }}> {project.name}</div>
                    </div>
                    <div className="p-3" style={{ overflowY: 'auto', overflowX: 'hidden' }}>
                      {project.function_map.map(single_function => (
                        <div className="rounded broder mx-2 mb-2 d-inline-block tags px-2">
                          <i className="fa fa-network-wired" /> {single_function.label}
                        </div>
                      ))}
                    </div>
                  </div>
                </Link>
              ))}
              <Link onClick={() => toggleModal('new_project')}>
                <div
                  className="  bg-light rounded d-flex flex-column "
                  style={{ height: '100%', opacity: '0.8', border: '1px dashed black' }}
                >
                  <div
                    className="border-top p-2 px-3 h5  text-drak  mb-0"
                    style={{
                      margin: '-1px',
                      minHeight: '20px',
                      borderRadius: '0px 0px 3px 3px',
                    }}
                  >
                    <div style={{ textTransform: 'capitalize' }}> New Project</div>
                    <div class="fioriIcon">
                      <i class="fa fa-plus" />
                    </div>
                  </div>
                  <div className="p-3" />
                </div>
              </Link>
            </div>
          </div>
        </div>
      </MainLayout>
    );
  }
}

const mapStateToProps = state => ({ projects: state.diagram.projects });

const mapDispatchToProps = { toggleModal };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StartUp);
