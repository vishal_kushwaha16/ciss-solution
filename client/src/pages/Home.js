import React, { Component } from 'react';
import { DiagramEngine, DiagramModel, DefaultNodeModel, DiagramWidget } from 'storm-react-diagrams';
import NewSystem from './../components/NewSystem';
import NewConnection from './../components/NewConnection';
import './../style/demo.scss';
import { connect } from 'react-redux';
import { setModal } from './../redux/actions/modal-actions';
import { setDiagram, setProject } from './../redux/actions/diagram-actions';
import MainLayout from './../main/MainLayout'; /* in ES 6 */
import domtoimage from 'dom-to-image';

class Home extends Component {
  componentWillMount() {
    this.props.setProject(this.props.match.params.id);

    //1) setup the diagram engine
    this.engine = new DiagramEngine();
    this.engine.installDefaultFactories();

    //2) setup the diagram model
    this.model = new DiagramModel();
    this.connections = {};
    this.systems = {};

    this.addListeners();
    //5) load model into engine
    this.engine.setDiagramModel(this.model);
  }

  componentDidMount() {
    setTimeout(() => {
      this.loadDiagram();
    }, 1000);
  }
  loadDiagram = isHook => {
    if (this.props.diagram) {
      this.model = new DiagramModel();
      this.model.deSerializeDiagram(this.props.diagram, this.engine);
      this.addListeners();

      this.engine.setDiagramModel(this.model);
      this.engine.repaintCanvas();
      if (!isHook) this.forceUpdate();
      this.engine.repaintCanvas();
      // move system type to next line
      /*document
        .querySelectorAll('.srd-default-node__name ')
        .forEach(label => (label.innerHTML = label.innerHTML.replace('(', '<br/>(')));*/
    }
  };

  addListeners = () => {
    this.model.addListener({
      linksUpdated: ({ link, isCreated }) => {
        if (!link) {
          return false;
        }
        const id = link.id;
        if (link.sourcePort) {
          // link has been connected, ask for entities here
          this.connections[id] = {
            from: link.sourcePort.id,
          };
        }

        link.addListener({
          targetPortChanged: ({ port }) => {
            this.connections[id].to = port.id;
            this.saveDiagram();
            this.props.setModal('new_connection', true, { id });
          },
        });
      },
    });
  };

  addSystem = details => {
    const node = new DefaultNodeModel(details.name, 'rgb(60, 60, 60)');
    details.entity.map(name => {
      if (node.addInOutPort) {
        // modified library
        node.addInOutPort(name.label);
      } else {
        node.addIn(name.label);
        node.addOut(name.label);
      }
    });
    node.setPosition(100, 100);
    this.systems[node.id] = { node, ...details };
    this.model.addAll(node);
    this.engine.repaintCanvas();
  };

  onRefresh = () => {
    setTimeout(this.loadDiagram, 0);
  };

  saveDiagram = () => {
    const diagramState = this.model.serializeDiagram();
    this.props.setDiagram(diagramState);
  };

  render() {
    return (
      <MainLayout title={this.props.project.name}>
        <div className="Home">
          <div className="srd-demo-workspace">
            <div className="srd-demo-workspace__toolbar d-flex ">
              <div className="mr-auto" />
              <div className="ml-auto">
                <NewSystem save={this.addSystem} />
                <NewConnection buttonLabel="New Connection" onRefresh={this.onRefresh} />

                <button
                  onClick={() => {
                    domtoimage
                      .toJpeg(document.getElementById('canvas-diagram'), { quality: 0.95 })
                      .then(function(dataUrl) {
                        var link = document.createElement('a');
                        link.download = 'integrtr-digital-transformer-diagram.jpeg';
                        link.href = dataUrl;
                        link.click();
                      });
                  }}
                >
                  <i class="fas fa-file-download fa-sm mr-1   " />
                  Export
                </button>
              </div>
            </div>
            <div id="canvas-diagram" className="srd-demo-workspace__content">
              <DiagramWidget className="srd-demo-canvas" diagramEngine={this.engine} />
            </div>
          </div>
        </div>
      </MainLayout>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const {
    match: {
      params: { id },
    },
  } = ownProps;

  return { diagram: state.diagram.diagram, project: state.diagram.projects[id] };
};

const mapDispatchToProps = { setDiagram, setModal, setProject };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
