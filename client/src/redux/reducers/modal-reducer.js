import { MODAL_TOGGLE, MODAL_SET } from './../actions/modal-actions';

const initialState = {
  // new_project: true,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case MODAL_TOGGLE:
      state[payload.name] = payload.name in state ? !state[payload.name] : true;
      return { ...state };
    case MODAL_SET:
      return { ...state, ...payload };

    default:
      return state;
  }
};
