import { SET_DIAGRAM, SET_PROJECT, CREATE_PROJECT } from './../actions/diagram-actions';
const initialState = {
  projects: {},
  selected_project: 'startup',
  project_store: {},
  diagram: null,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_DIAGRAM:
      return {
        ...state,
        project_store: { ...state.project_store, [state.selected_project]: { ...payload } },
        diagram: { ...payload },
      };
    case SET_PROJECT:
      return {
        ...state,
        selected_project: payload,
        diagram: state.project_store[payload] || null,
      };

    case CREATE_PROJECT:
      return {
        ...state,
        diagram: payload.objNewModel || null,
        selected_project: payload.details.id,
        projects: { ...state.projects, [payload.details.id]: payload.details },
        project_store: { ...state.project_store, [payload.details.id]: { ...payload.objNewModel } },
      };

    default:
      return state;
  }
};
