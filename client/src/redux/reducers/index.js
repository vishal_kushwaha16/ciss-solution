import { combineReducers } from 'redux';
import diagramReducer from './diagram-reducer';
import modalReducer from './modal-reducer';

export default combineReducers({
  diagram: diagramReducer,
  modal: modalReducer,
});
