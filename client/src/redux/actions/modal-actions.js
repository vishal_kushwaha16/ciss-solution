export const MODAL_TOGGLE = 'MODAL_TOGGLE';
export const MODAL_SET = 'MODAL_SET';

export const toggleModal = name => ({
  type: MODAL_TOGGLE,
  payload: { name },
});

export const setModal = (name, state, data) => ({
  type: MODAL_SET,
  payload: { [name]: state, [`${name}-data`]: data },
});
