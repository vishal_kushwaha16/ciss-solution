import { DiagramModel, DefaultNodeModel } from 'storm-react-diagrams';
import uuid from 'uuid/v1';

export const SET_PROJECT = 'SET_PROJECT';
export const CREATE_PROJECT = 'CREATE_PROJECT';
export const SET_DIAGRAM = 'SET_DIAGRAM';

export const setDiagram = payload => ({
  type: SET_DIAGRAM,
  payload,
});

export const setProject = payload => ({
  type: SET_PROJECT,
  payload,
});

export const createProject = details => {
  const newModel = new DiagramModel();
  details.id = uuid();

  details.function_map.map((item, inc) => {
    const systemType = details.systems_current[item.value].label;
    const node = new DefaultNodeModel(`${item.label} `, 'rgb(60, 60, 60)', systemType, 'source');

    details.entity[item.value].map(name => {
      if (node.addInOutPort) {
        // modified library
        node.addInOutPort(name.label);
      } else {
        node.addInPort(name.label);
        node.addOutPort(name.label);
      }
    });

    node.setPosition(100, 100 + inc * 120);
    newModel.addAll(node);
  });

  details.function_map.map((item, inc) => {
    const systemType = details.systems_new[item.value].label;
    const node = new DefaultNodeModel(`${item.label}`, 'rgb(60, 60, 60)', systemType, 'target');

    details.entity[item.value].map(name => {
      if (node.addInOutPort) {
        node.addInOutPort(name.label);
      } else {
        node.addInPort(name.label);
        node.addOutPort(name.label);
      }
    });

    node.setPosition(600, 100 + inc * 120);
    newModel.addAll(node);
  });

  const objNewModel = newModel.serializeDiagram();

  return {
    type: CREATE_PROJECT,
    payload: {
      objNewModel,
      details,
    },
  };
};
