Client side development repository for CISS - Solution Designer

## Developing Application

### `npm start` or `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br> You will also see any lint errors in the console.

### `npm run build` or `yarn build`

Builds the app for production to the `build` folder.<br>

> NOTE: For final deployment ( to be reflected in hosted app), the code should be moved to `../public` folder in root.

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
