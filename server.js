// Dependencies
// -----------------------------------------------------
var express = require('express');
var port = process.env.PORT || 1111;
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var app = express();
var path = require('path');
// Express Configuration
// ---------------------------------------------------

// Logging and Parsing
app.use(express.static(__dirname + '/public')); // sets the static files location to public
app.use(morgan('dev')); // log with Morgan
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.urlencoded({ extended: true })); // parse application/x-www-form-urlencoded
app.use(bodyParser.text()); // allows bodyParser to look at raw text
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json

// Handles any requests that don't match the ones above
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname + '/public/index.html'));
});

app.use(methodOverride());

// Listen
// -------------------------------------------------------
app.listen(port);
console.log('App listening on port ' + port);
